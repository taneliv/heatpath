# Heat Path

This is a single page web application that can be used to plot paths on
a map. The paths are rendered using circles of color, indicating "heat."

The application can be launched in a browser by using one of

    firefox --private-window map.html
    chrome --incognito map.html

or other method, for example by clicking on the file's icon.

Once appliction has started, one can mark waypoints of the path by
simply clicking on the map. The buttons provide their labeled actions.

## Testing

There is a (Python) test script. A typical way to run it would be:

    pip install -r requirements.txt
    rm -f /tmp/paths.json  # to rid of old results
    python test.py

The test setup depends on selenium's chromedriver (and hence Chrome) for
no particular reason. Installing the driver is at present outside the
scope of this document, covered to some extent here:
https://sites.google.com/a/chromium.org/chromedriver/getting-started

For future, mocha.js or similar should be used for unit testing.
However, this will require a considerable rewrite of the application, to
make it testable.

## Structure

There is no server component besides the Google Maps API use. In
particular, any paths are saved only locally, in user's browser.

The code is inside the HTML file within two script elements. The first
one defines all the functionality and variables, and the second one
simply calls to them.

There paths are stored in a local storage variable called "paths". This
is also used to generate the download. The internal structure is
straight forward JSON representation of the coordinates, where each path
is an element in a top level array.

There are four global variables:
 * map is a google.maps.Map object and necessary for interactions
 * path is the path currently being defined by the user
 * heatmap is the potentially visible heatmap representation of the
   paths
 * markers are the markers of the path currently being defined

Very little concern has been placed on efficiency. When the user clicks
on the map to extend the current path, the markers and Polyline
representing the path will be redrawn. When the user clicks to toggle
heatmap, all the coordinates in all the paths are inserted to the
HeatmapLayer and made visible, or if currently visible, made invisible
and eventually discarded.

## Choice of Technologies

Aspirationally this should be a small implementation, with minimal
external dependencies. Selenium's dependency on python and browser
drivers is unfortunate and it should perhaps be replaced with another
tool, like phantomjs.
