import json
import os
import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains


TEMP_DIR = "/tmp/"
FNAME = os.path.join(TEMP_DIR, "paths.json")
assert not os.path.exists(FNAME)

chrome_options = webdriver.ChromeOptions()
chrome_options.add_experimental_option("prefs", {"download.default_directory": TEMP_DIR})
driver = webdriver.Chrome('/usr/lib/chromium-browser/chromedriver', chrome_options=chrome_options)
driver.get('file:///home/taneli/Source/heatpath/map.html')
# My computer is a potato, and I don't want to depend on google maps js lib internal structures
time.sleep(15)
element = driver.find_elements_by_css_selector('#map')[0]
actions = ActionChains(driver)
actions.move_to_element_with_offset(element, 204, 84)
actions.click()
actions.release()
actions.perform()
time.sleep(2)
element = driver.find_elements_by_css_selector('#map')[0]
actions.move_by_offset(244, 160)
actions.click()
actions.release()
actions.perform()
element = driver.find_element_by_css_selector('#download-button')
element.click()
# It still is a potato
time.sleep(10)
assert os.path.exists(FNAME)
result = json.load(open(FNAME))
driver.quit()

assert len(result) == 1
path0 = result[0]
assert "positions" in path0
assert "markers" in path0
positions = path0["positions"]
assert len(positions) == 2
pos0 = positions[0]
pos1 = positions[1]
assert "lat" in pos0 and "lng" in pos0
assert "lat" in pos1 and "lng" in pos1
lat0 = pos0["lat"]
lng0 = pos0["lng"]
lat1 = pos1["lat"]
lng1 = pos1["lng"]
assert int(lat0) == 60 and int(lng0) == 24
assert int(lat1) == 60 and int(lng1) == 24
print("SUCCESS")
